<?php

namespace AppBundle\Controller;

use AppBundle\Action\SmfpFront\SmfpFrontHelloAction;        // File chứa hàm <hello>
use Symfony\Bundle\FrameworkBundle\Controller\Controller;   // Thư viện Controller của Core
use Symfony\Component\Routing\Annotation\Route;         // Thư viện cho viện khai báo Route bằng Annotation

class SmfpFrontController extends Controller            // Kế thừa Controller của Core
{

    /**
     * @Route("/hello" , name = "hello")                // Khai báo Route </hello> bằng Annotation
     */
    public function helloAction()
    {
        return SmfpFrontHelloAction::hello($this);      // Gọi đến hàm <hello>
    }
}