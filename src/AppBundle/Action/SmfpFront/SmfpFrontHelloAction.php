<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/06/2018
 * Time: 10:50 SA
 */

namespace AppBundle\Action\SmfpFront;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SmfpFrontHelloAction extends Controller       // Kế thừa Controller của Core
{

    public static function hello(Controller $controller)
    {
        return $controller->render('@front/smfp-front/front-smfp-hello.html.twig');  // Render template từ controller của <SmfpFrontController>
    }
}